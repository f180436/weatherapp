package com.example.assignment05;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CityDao {
    @Query("SELECT * FROM city")
    List<CityEntity> getAll();

    @Query("SELECT * FROM city WHERE city_name=:city")
    public List<CityEntity> findCityWithName(String city);

    @Insert
    void insertAll(CityEntity... cityEntities);
}
