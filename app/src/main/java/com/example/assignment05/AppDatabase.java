package com.example.assignment05;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {CityEntity.class, WeatherInfoEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CityDao cityDao();
    public abstract WeatherInfoDao weatherInfoDao();
}
